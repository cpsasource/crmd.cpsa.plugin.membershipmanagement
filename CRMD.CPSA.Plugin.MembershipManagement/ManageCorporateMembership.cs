﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using CRMD.CPSA.Plugin.MembershipManagement.Models;
using CRMD.CPSA.Plugin.MembershipManagement.Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;

namespace CRMD.CPSA.Plugin.MembershipManagement
{
    public class ManageCorporateMembership : PluginBase, IPlugin
    {
        // update of SalesOrder, Post-Op
        public void Execute(IServiceProvider serviceProvider)
        {
            base.Initialize(serviceProvider);

            if (this.TargetEntity == null)
                return;

            if (this.Context.Depth > 2)
                return;

            this.Tracing.Trace("*** Begin : " + this.TargetEntity.Id + "***");
            int renewalMembershipProductType = 790410000;

            Enums.MembershipStatus membershipStatus = Enums.MembershipStatus.Current;

            SalesOrder order = SalesOrderService.Retrieve(this.Service, this.TargetEntity.Id);

            if (order != null && order.WillProcessCorporateMembership)
            {
                Membership currentMembership = null;

                // get latest corporate membership
                if (order.MembershipLookupId  == Guid.Empty)
                {
                    currentMembership = MembershipService.GetLatestCorporateMembership(this.Service, order.CorporateAccount.Id);
                }
                else
                {
                    currentMembership = MembershipService.GetMembershipById(this.Service, order.MembershipLookupId);
                }
                this.Tracing.Trace("Retrieved account's current corporate membership");

                // Check the products in the order if it has Corporate Renewal Membership
                this.Tracing.Trace("** Getting product lines in the order **");
                List<SalesOrderDetail> lstLineItems = SalesOrderService.RetrieveProductLineItems(this.Service, this.TargetEntity.Id);

                // set the estimated quantity if user is purchasing Corporate Renewal Membership and Corporate Membership at the same time (quantity of both)
                int estimatedQuantity = 0;            
                foreach (var product in lstLineItems)
                {
                    foreach (var checkingProduct in lstLineItems)
                    {
                        // check if those two products are 1 pair respectively and the Renewal Product quantity should greater than the number of purchased
                        if (product.RenewalProductId == checkingProduct.ProductNameId && (int)checkingProduct.Quantity > currentMembership.NumberOfPurchasedMemberships)
                            estimatedQuantity = (int)product.Quantity + currentMembership.NumberOfPurchasedMemberships;                    
                    }            
                }
                this.Tracing.Trace("-- Got an estimated quantity for the renewal product which is: " + estimatedQuantity);

                this.Tracing.Trace("** End getting product lines in the order **");
           

                foreach (var product in lstLineItems)
                {
                    this.Tracing.Trace($"Product Name: {product.ProductName}");
                    this.Tracing.Trace($"Product ID: {product.Id}");
                    this.Tracing.Trace($"Product Name ID: {product.ProductNameId}");
                    this.Tracing.Trace($"Product Quantity: {product.Quantity}");
                    this.Tracing.Trace($"Product Renewal ID: {product.RenewalProductId}");
                    this.Tracing.Trace($"Is Product Unit Group Corporate: {product.IsProductUnitGroupCorporate}");
                    this.Tracing.Trace($"Product Type Code: {product.ProductTypeCode}");

                    this.Tracing.Trace("Plugin will process Corporate Membership");
                    this.Tracing.Trace($"Product Number: {product.ProductNumber}");

                    Membership latestMembership = null;
                    bool isNewMembership = false;

                    // allow the plugin to update the record beforehand even the quantity is not correct if this is the Renewal Product and other is Corporate New Membership
                    bool allowToUpdate = false;
                    foreach (var checkingProduct in lstLineItems)
                    {
                        if (product.ProductNameId == checkingProduct.RenewalProductId)
                            allowToUpdate = true;                        
                    }
                    this.Tracing.Trace("-- Allow to Update beforehand: " + allowToUpdate);

                    if (currentMembership != null)
                    {
                        membershipStatus = EnumHelper.GetEnum<Enums.MembershipStatus>(currentMembership.MembershipStatus);
                        this.Tracing.Trace($"Account's corporate membership status is '{membershipStatus}'");
                    }

                    if (currentMembership != null && currentMembership.IsCurrentOrGracePeriod)
                    {
                        // update current membership
                        bool isRenew = product.ProductTypeCode == renewalMembershipProductType; //|| currentMembership.MembershipStatus == (int)Enums.MembershipStatus.GracePeriod;

                        latestMembership = MembershipService.UpdateCorporateMembership(this.Service, this.Tracing, currentMembership, order, product, isRenew, allowToUpdate, estimatedQuantity);

                        // update child membership renewal
                        MembershipService.UpdateChildMembershipRenewal(this.Service, this.Tracing, currentMembership);
                    }
                    else
                    {
                        if (currentMembership == null)
                            this.Tracing.Trace("No corporate membership linked to Account");

                        this.Tracing.Trace("Creating new corporate membership for Account");

                        // validate product
                        // renewal is not allowed
                        this.Tracing.Trace("Validate product");
                        if (product.ProductTypeCode == renewalMembershipProductType)
                        {
                            // throw a friendly message
                            if (currentMembership == null)
                                throw new InvalidPluginExecutionException("Renewal is not allowed for new membership.");
                            else
                                throw new InvalidPluginExecutionException($"Renewal is not allowed for {membershipStatus} membership.");
                        }

                        // create Corporate Membership
                        isNewMembership = true;
                        Membership newMembership = MembershipService.CreateNewCorporateMembership(this.Service, order, product);
                        if (newMembership != null && newMembership.Id != Guid.Empty)
                        {
                            latestMembership = newMembership;
                            this.Tracing.Trace("Created a new membership for this account : " + newMembership.Id);
                        }
                    }

                    if (latestMembership != null)
                    {
                        // update Account Current Membership 
                        AccountService.UpdateCurrentMembership(this.Service, order.CorporateAccount.Id, latestMembership.Id);
                        this.Tracing.Trace("Account updated with current membership details : " + order.CorporateAccount.Id);

                        if(order.MembershipLookupId == Guid.Empty)
                        {
                            // update Order with Corporate Membership lookup
                            SalesOrderService.UpdateOrder(this.Service, this.Tracing, order.Id, latestMembership, product, isNewMembership);
                            this.Tracing.Trace("Order updated with corporate membership lookup : " + latestMembership.Id);
                        }
                        else
                        {
                            SalesOrderService.ActivateOrder(this.Service, order.Id);
                            SalesOrderService.UpdateMembershipLineItemStartEndDate(this.Service, this.Tracing, latestMembership, product, false);
                            SalesOrderService.FulfillOrder(this.Service, order.Id);
                            this.Tracing.Trace("Order line item start/end date updated.");
                        }
                    }
                }

            }
            else
            {
                this.Tracing.Trace(String.Format("Criteria not met for processing corporate membership. Completed : {0}, Has Corporate Account: {1}, Has Corporate Membership Line Item: {2}",
                                                 order.IsComplete ? "Y" : "N",
                                                 order.CorporateAccount != null ? "Y" : "N",
                                                 order.CorporateMembershipLineItem != null ? "Y" : "N"));
            }

            this.Tracing.Trace("*** End of plugin execution ***");
        }
    }
}
