﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.MembershipManagement.Models
{
    public class SalesOrder
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public Guid MembershipLookupId { get; set; }

        public Account CorporateAccount { get; set; }

        public List<SalesOrderDetail> Products { get; set; }
        public SalesOrderDetail IndividualMembershipLineItem
        {
            get
            {
                if (Products != null)
                {
                    return Products.FirstOrDefault(p => p.IsProductUnitGroupIndividual && p.Quantity > 0);
                }

                return null;
            }
        }

        public SalesOrderDetail CorporateMembershipLineItem
        {
            get
            {
                if(Products != null)
                {
                    return Products.FirstOrDefault(p => p.IsProductUnitGroupCorporate && p.Quantity > 0);
                }

                return null;
            }
        }
                
        public bool IsAutoRenewMembership { get; set; }
        public bool IsComplete { get; set; }

        public bool WillProcessIndividualMembership
        {
            get
            {
                return IsComplete &&
                       IndividualMembershipLineItem != null;
            }
        }      

        public bool WillProcessCorporateMembership
        {
            get
            {
                return IsComplete &&
                       CorporateAccount != null &&
                       CorporateMembershipLineItem != null;
            }
        }

        public decimal TotalAmount { get; set; }

        public string ApprovalCode { get; set; }
        public string Name { get; set; }

        public DateTime ApprovalDateAndTime { get; set; }

        public Enums.OrderPaymentType PaymentType { get; set; }
        public string CheckNumber { get; set; }

        public int OrderDiscountMembershipType { get; set; }
        public int OrderDiscountMembershipCategory { get; set; }

        public int IndividualMembershipType
        {
            get
            {
                if(IndividualMembershipLineItem != null)
                {
                    //Hierarchy: (1) Account Company Discount (Subscriber Paid), (2) Order Discount, (2) Line Item Discount, (3) Line Item Product
                    return CorporateAccount != null && CorporateAccount.CompanyDiscount > 0m ? (int)Enums.MembershipType.SubscriberPaid :
                                                                                               (OrderDiscountMembershipType > 0 ? OrderDiscountMembershipType :
                                                                                                                                 (IndividualMembershipLineItem.DiscountMembershipType > 0 ? IndividualMembershipLineItem.DiscountMembershipType :
                                                                                                                                                                                            (IndividualMembershipLineItem.ProductMembershipType > 0 ? IndividualMembershipLineItem.ProductMembershipType : (int)Enums.MembershipType.Subscriber)));
                }

                return 0;
            }
        }

        public int IndividualMembershipCategory
        {
            get
            {
                if(IndividualMembershipLineItem != null)
                {
                    //Hierarchy: (1) Order Discount, (2)  Line Item Discount, (3) Line Item Product
                    return OrderDiscountMembershipCategory > 0 ? OrderDiscountMembershipCategory : 
                                                                 (IndividualMembershipLineItem.DiscountMembershipCategory > 0 ? IndividualMembershipLineItem.DiscountMembershipCategory : 
                                                                                                                                (IndividualMembershipLineItem.ProductMembershipCategory > 0 ? IndividualMembershipLineItem.ProductMembershipCategory : (int)Enums.MembershipCategory.Member));
                }

                return 0;
            }
        }

        public int CorporateMembershipType
        {
            get
            {
                if (CorporateMembershipLineItem != null)
                {
                    return CorporateMembershipLineItem.DiscountMembershipType > 0 ? CorporateMembershipLineItem.DiscountMembershipType : 
                                                                                    (CorporateMembershipLineItem.ProductMembershipType > 0 ? CorporateMembershipLineItem.ProductMembershipType : (int)Enums.MembershipType.OrganizationPaid);
                }

                return 0;
            }
        }

        public int CorporateMembershipCategory
        {
            get
            {
                if (CorporateMembershipLineItem != null)
                {
                    return CorporateMembershipLineItem.DiscountMembershipCategory > 0 ? CorporateMembershipLineItem.DiscountMembershipCategory : 
                                                                                        (CorporateMembershipLineItem.ProductMembershipCategory > 0 ? CorporateMembershipLineItem.ProductMembershipCategory : (int)Enums.MembershipCategory.Member);
                }

                return 0;
            }
        }
    }
}
