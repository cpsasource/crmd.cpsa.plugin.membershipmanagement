﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using System;

namespace CRMD.CPSA.Plugin.MembershipManagement.Models
{
    public class Membership
    {
        public Guid Id { get; set; }
        public Guid ContactId { get; set; }
        public Guid AccountId { get; set; }
        public Guid MembershipProductLookupId { get; set; }

        public int MembershipStatus { get; set; }
        public int MembershipCategory { get; set; }
        public int MembershipType { get; set; }
        public int MembershipAction { get; set; }
        public int NumberOfPurchasedMemberships { get; set; }
        public int NumberOfActiveMembership { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime RenewalDate { get; set; }

        public bool IsAutoRenewMembership { get; set; }

        public bool IsCurrentOrGracePeriod
        {
            get
            {
                return MembershipStatus == (int)Enums.MembershipStatus.Current || MembershipStatus == (int)Enums.MembershipStatus.GracePeriod;
            }
        }
    }
}
