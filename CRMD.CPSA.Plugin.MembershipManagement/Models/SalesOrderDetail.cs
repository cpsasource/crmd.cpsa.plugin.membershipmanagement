﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using System;

namespace CRMD.CPSA.Plugin.MembershipManagement.Models
{
    public class SalesOrderDetail
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductNumber { get; set; }

        public string ProductUnitGroupName { get; set; }
                
        public decimal ProductDefaultUnit { get; set; }
        public decimal Quantity { get; set; }
        public decimal PricePerUnit { get; set; }

        public int ProductMembershipTerm
        {
            get
            {
                return Decimal.ToInt32(ProductDefaultUnit);
            }
        }

        public bool IsProductUnitGroupIndividual
        {
            get
            {
                return !string.IsNullOrEmpty(ProductUnitGroupName) && ProductUnitGroupName == Constants.UnitGroup_Individual;
            }
        }

        public bool IsProductUnitGroupCorporate
        {
            get
            {
                return !string.IsNullOrEmpty(ProductUnitGroupName) && ProductUnitGroupName == Constants.UnitGroup_Corporate;
            }
        }

        public int ProductMembershipCategory { get; set; }
        public int ProductMembershipType { get; set; }
        public int DiscountMembershipCategory { get; set; }
        public int DiscountMembershipType { get; set; }
        public int ProductTypeCode { get; set; }
        public Guid RenewalProductId { get; set; }
        public Guid ProductNameId { get; set; }
        public DateTime ProRateStartDate { get; set; }
    }
}
