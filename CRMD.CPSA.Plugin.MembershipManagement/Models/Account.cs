﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.MembershipManagement.Models
{
    public class Account
    {
        public Guid Id { get; set; }
        public Guid DesignatedAdminId { get; set; }
        public decimal CompanyDiscount { get; set; }
        public bool HasCouponCodes { get; set; }
    }
}
