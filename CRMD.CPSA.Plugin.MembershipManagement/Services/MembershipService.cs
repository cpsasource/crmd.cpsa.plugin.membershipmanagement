﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using CRMD.CPSA.Plugin.MembershipManagement.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.MembershipManagement.Services
{
    public static class MembershipService
    {
        #region Individual Membership
        public static Membership GetMembershipById(IOrganizationService service, Guid membershipId)
        {
            var result = service.Retrieve(Constants.crmd_membership, membershipId, new ColumnSet(Constants.crmd_membershiptype, Constants.crmd_membershiptype, Constants.crmd_membershipstatus, 
                                                                                                 Constants.crmd_membershipcategory, Constants.crmd_contactid, Constants.crmd_startdate, Constants.adx_renewaldate,
                                                                                                 Constants.adx_accountid, Constants.crmd_numactivememberships, Constants.crmd_numberpurchasedmemberships, 
                                                                                                 Constants.crmd_productlkup, Constants.crmd_autorenew));
            if (result != null)
            {
                return ToMembershipModel(result);
            }

            return null;
        }

        public static Membership GetLatestIndividualMembership(IOrganizationService service, Guid contactId)
        {
            if (contactId != Guid.Empty)
            {
                string query = @"<fetch mapping='logical' version='1.0' >
                                    <entity name='crmd_membership' >
                                        <attribute name='crmd_membershipid' />
                                        <attribute name='crmd_membershiptype' />
                                        <attribute name='crmd_membershipstatus' />
                                        <attribute name='crmd_membershipcategory' />
                                        <attribute name='crmd_contactid' />
                                        <attribute name='crmd_startdate' />
                                        <attribute name='adx_renewaldate' />
                                        <attribute name='crmd_productlkup' />
                                        <attribute name='crmd_autorenew' />
                                        <attribute name='crmd_membershipaction' />
                                        <filter type='and' >
                                            <condition attribute='statecode' operator='eq' value='" + Constants.StateCode_Active + @"' />
                                            <condition attribute='crmd_contactid' operator='eq' value='" + contactId.ToString() + @"' />
                                            <filter type='or'>
                                                <condition attribute='crmd_membershiptype' operator='eq' value='" + (int)Enums.MembershipType.Subscriber + @"' />
                                                <condition attribute='crmd_membershiptype' operator='eq' value='" + (int)Enums.MembershipType.SubscriberPaid + @"' />
                                            </filter>
                                        </filter>
                                        <order attribute='adx_renewaldate' descending='true' />
                                        <link-entity name='product' from='productid' to='crmd_productlkup'>
                                        </link-entity>
                                    </entity>
                                </fetch>";

                var results = service.RetrieveMultiple(new FetchExpression(query));
                if (results != null && results.Entities != null && results.Entities.Count > 0)
                {
                    return ToMembershipModel(results.Entities.FirstOrDefault());
                }
            }

            return null;
        }

        public static Membership UpdateIndividualMembershipAsLatestCurrent(IOrganizationService service, Membership currentMembership, SalesOrder order)
        {
            Entity membershipToUpdate = new Entity(Constants.crmd_membership, currentMembership.Id);

            if (currentMembership.MembershipCategory != order.IndividualMembershipCategory)
            {
                membershipToUpdate[Constants.crmd_membershipcategory] = new OptionSetValue(order.IndividualMembershipCategory);
                currentMembership.MembershipCategory = order.IndividualMembershipCategory;
            }

            if (currentMembership.MembershipType != order.IndividualMembershipType)
            {
                membershipToUpdate[Constants.crmd_membershiptype] = new OptionSetValue(order.IndividualMembershipType);
                currentMembership.MembershipType = order.IndividualMembershipType;
            }

            if (currentMembership.MembershipStatus != (int)Enums.MembershipStatus.Current)
            {
                membershipToUpdate[Constants.crmd_membershipstatus] = new OptionSetValue((int)Enums.MembershipStatus.Current);
                currentMembership.MembershipStatus = (int)Enums.MembershipStatus.Current;
            }

            if (currentMembership.MembershipProductLookupId != order.IndividualMembershipLineItem.ProductId)
            {
                membershipToUpdate[Constants.crmd_productlkup] = new EntityReference(Constants.product, order.IndividualMembershipLineItem.ProductId);
                currentMembership.MembershipProductLookupId = order.IndividualMembershipLineItem.ProductId;
            }
            
            if (currentMembership.MembershipAction != (int)Enums.MembershipAction.Renewal)
            {
                membershipToUpdate[Constants.crmd_membershipaction] = new OptionSetValue((int)Enums.MembershipAction.Renewal);
                currentMembership.MembershipAction = (int)Enums.MembershipAction.Renewal;
            }

            DateTime currentRenewalFirstDay = new DateTime(currentMembership.RenewalDate.Year, currentMembership.RenewalDate.Month, 1);
            DateTime newRenewalDate = currentMembership.RenewalDate.AddMonths(order.IndividualMembershipLineItem.ProductMembershipTerm);
            if (currentRenewalFirstDay != currentMembership.RenewalDate)
            {
                newRenewalDate = currentRenewalFirstDay.AddMonths(order.IndividualMembershipLineItem.ProductMembershipTerm + 1).AddDays(-1);
            }

            membershipToUpdate[Constants.adx_renewaldate] = newRenewalDate;
            currentMembership.RenewalDate = newRenewalDate;

            service.Update(membershipToUpdate);

            return currentMembership;
        }

        public static Membership CreateNewIndividualMembership(IOrganizationService service, SalesOrder order)
        {
            DateTime dtToday = DateTime.Now;
            DateTime dtStartDate = new DateTime(dtToday.Year, dtToday.Month, 1);

            Entity membership = new Entity(Constants.crmd_membership);
            membership[Constants.crmd_contactid] = new EntityReference(Constants.contact, order.CustomerId);
            membership[Constants.crmd_startdate] = dtStartDate;
            membership[Constants.adx_renewaldate] = dtStartDate.AddMonths(order.IndividualMembershipLineItem.ProductMembershipTerm + 1).AddDays(-1);
            membership[Constants.crmd_autorenew] = order.IsAutoRenewMembership;
            membership[Constants.crmd_membershipaction] = new OptionSetValue((int)Enums.MembershipAction.New);
            membership[Constants.crmd_membershipstatus] = new OptionSetValue((int)Enums.MembershipStatus.Current);
            membership[Constants.crmd_productlkup] = new EntityReference(Constants.product, order.IndividualMembershipLineItem.ProductId);
            membership[Constants.crmd_membershiptype] = new OptionSetValue(order.IndividualMembershipType);
            membership[Constants.crmd_membershipcategory] = new OptionSetValue(order.IndividualMembershipCategory);

            membership.Id = service.Create(membership);

            return ToMembershipModel(membership);
        }
        #endregion

        #region Corporate Membership
        public static Membership GetLatestCorporateMembership(IOrganizationService service, Guid accountId)
        {
            if (accountId != Guid.Empty)
            {
                string query = @"<fetch mapping='logical' version='1.0' >
                                    <entity name='crmd_membership' >
                                        <attribute name='crmd_membershipid' />
                                        <attribute name='crmd_membershiptype' />
                                        <attribute name='crmd_membershipstatus' />
                                        <attribute name='crmd_membershipcategory' />
                                        <attribute name='adx_accountid' />
                                        <attribute name='crmd_startdate' />
                                        <attribute name='adx_renewaldate' />
                                        <attribute name='crmd_productlkup' />
                                        <attribute name='crmd_autorenew' />
                                        <attribute name='crmd_membershipaction' />
                                        <attribute name='crmd_numberpurchasedmemberships' />
                                        <attribute name='crmd_numactivememberships' />
                                        <filter type='and' >
                                            <condition attribute='statecode' operator='eq' value='" + Constants.StateCode_Active + @"' />
                                            <condition attribute='adx_accountid' operator='eq' value='" + accountId.ToString() + @"' />
                                            <condition attribute='crmd_membershiptype' operator='eq' value='" + (int)Enums.MembershipType.OrganizationPaid + @"' />
                                            <condition attribute='crmd_iscompanymembership' operator='eq' value='1' />
                                        </filter>
                                        <order attribute='adx_renewaldate' descending='true' />
                                        <link-entity name='product' from='productid' to='crmd_productlkup'>
                                        </link-entity>
                                    </entity>
                                </fetch>";

                var results = service.RetrieveMultiple(new FetchExpression(query));
                if (results != null && results.Entities != null && results.Entities.Count > 0)
                {
                    return ToMembershipModel(results.Entities.FirstOrDefault());
                }
            }

            return null;
        }

        public static Membership UpdateCorporateMembership(IOrganizationService service, ITracingService tracing, Membership currentMembership, SalesOrder order, SalesOrderDetail orderProduct = null, bool isRenew = false, bool allowToUpdate = false, int estimatedQuantity = 0)
        {
            tracing.Trace($"Update account's membership: {currentMembership.Id}.");
            tracing.Trace($"Corporate membership renewal = {isRenew}.");

            Entity membershipToUpdate = new Entity(Constants.crmd_membership, currentMembership.Id);
            DateTime renewalDate = currentMembership.RenewalDate;
            //SalesOrderDetail salesOrderDetail = order.CorporateMembershipLineItem;
            int quantity = Convert.ToInt32(orderProduct.Quantity),
                currentActive = currentMembership.NumberOfActiveMembership,
                currentPurchased = currentMembership.NumberOfPurchasedMemberships;
            
            tracing.Trace($"---Current Status = {EnumHelper.GetEnum<Enums.MembershipStatus>(currentMembership.MembershipStatus)}.");
            tracing.Trace($"---Quantity = {quantity}.");
            tracing.Trace($"---Current Active = {currentActive}.");
            tracing.Trace($"---Current Purchased = {currentPurchased}.");

            Enums.MembershipStatus status = Enums.MembershipStatus.Current;


            int CorporateMembershipCategory =  orderProduct.DiscountMembershipCategory > 0 ? orderProduct.DiscountMembershipCategory :
                                               (orderProduct.ProductMembershipCategory > 0 ? orderProduct.ProductMembershipCategory : (int)Enums.MembershipCategory.Member);


            membershipToUpdate[Constants.crmd_membershipcategory] = new OptionSetValue(CorporateMembershipCategory);
            currentMembership.MembershipCategory = CorporateMembershipCategory;

            int CorporateMembershipType = orderProduct.DiscountMembershipType > 0 ? orderProduct.DiscountMembershipType :
                                          (orderProduct.ProductMembershipType > 0 ? orderProduct.ProductMembershipType : (int)Enums.MembershipType.OrganizationPaid);

            membershipToUpdate[Constants.crmd_membershiptype] = new OptionSetValue(CorporateMembershipType);
            currentMembership.MembershipType = CorporateMembershipType;

            membershipToUpdate[Constants.crmd_productlkup] = new EntityReference(Constants.product, orderProduct.ProductId);
            currentMembership.MembershipProductLookupId = orderProduct.ProductId;

            membershipToUpdate[Constants.crmd_membershipstatus] = new OptionSetValue((int)status);
            //currentMembership.MembershipStatus = (int)status;

            membershipToUpdate[Constants.crmd_membershipaction] = new OptionSetValue((int)Enums.MembershipAction.Renewal);
            currentMembership.MembershipAction = (int)Enums.MembershipAction.Renewal;

            if (isRenew)
            {
                // validate purchased membership

                if (!allowToUpdate)
                {
                    // Dont allow user to buy Renewal less than Purchased Memberships if the Membership Status is Current
                    if (currentMembership.MembershipStatus == (int)Enums.MembershipStatus.Current && quantity < currentPurchased)
                    {
                        throw new InvalidPluginExecutionException($"You can not renew less during 'Current', only in 'Grace Period'. Please wait until {currentMembership.RenewalDate}.");
                    }

                    // Dont allow user to buy Renewal more than Purchased Memberships if the Membership Status is Current
                    if (currentMembership.MembershipStatus == (int)Enums.MembershipStatus.Current && quantity > currentPurchased)
                    {
                        throw new InvalidPluginExecutionException($"You must add Corporate - New Memberships with the renewal product.");
                    }

                    // ie. Active members: 5, Purchased Members: 10 -> quantity should be 5 - 10 only
                    if (quantity < currentActive || quantity > currentPurchased)
                    {
                        // throw a friendly message
                        throw new InvalidPluginExecutionException($"{orderProduct.ProductName} quantity should be {currentActive} to {currentPurchased}.");
                    }
                    
                }
                else
                {
                    if (estimatedQuantity != quantity)
                        throw new InvalidPluginExecutionException("Number of seats must be equal to the renewal quantity.");
                }
               
                currentPurchased = 0;
                DateTime tempDate = new DateTime(currentMembership.RenewalDate.Year, currentMembership.RenewalDate.Month, 1);
                renewalDate = tempDate.AddMonths(orderProduct.ProductMembershipTerm + 1).AddDays(-1);
            }

            int numOfPurchasedMemberships = 0;
            if (estimatedQuantity > 0)
            {
                numOfPurchasedMemberships = estimatedQuantity;
            }
            else
            {
                numOfPurchasedMemberships = currentPurchased + quantity;               
            }

            membershipToUpdate[Constants.crmd_numberpurchasedmemberships] = numOfPurchasedMemberships;
            currentMembership.NumberOfPurchasedMemberships = numOfPurchasedMemberships;

            membershipToUpdate[Constants.adx_renewaldate] = renewalDate;
            currentMembership.RenewalDate = renewalDate;

            service.Update(membershipToUpdate);

            tracing.Trace($"Purchased Members: {quantity}");
            tracing.Trace($"Renewal Date: {renewalDate}");
            tracing.Trace($"Membership Status: {status}");
            tracing.Trace($"Updated account's latest membership: {currentMembership.Id}");

            return currentMembership;
        }

        public static Membership CreateNewCorporateMembership(IOrganizationService service, SalesOrder order, SalesOrderDetail product)
        {
            if (product.IsProductUnitGroupCorporate)
            {
                DateTime dtToday = DateTime.Now;
                DateTime dtStartDate = new DateTime(dtToday.Year, dtToday.Month, 1);

                Entity membership = new Entity(Constants.crmd_membership);
                membership[Constants.adx_accountid] = new EntityReference(Constants.account, order.CorporateAccount.Id);
                membership[Constants.crmd_startdate] = dtStartDate;
                membership[Constants.adx_renewaldate] = dtStartDate.AddMonths(product.ProductMembershipTerm + 1).AddDays(-1);
                membership[Constants.crmd_membershipaction] = new OptionSetValue((int)Enums.MembershipAction.New);
                membership[Constants.crmd_membershipstatus] = new OptionSetValue((int)Enums.MembershipStatus.Current);
                membership[Constants.crmd_productlkup] = new EntityReference(Constants.product, product.ProductId);
                membership[Constants.crmd_membershiptype] = new OptionSetValue((int)Enums.MembershipType.OrganizationPaid);
                membership[Constants.crmd_membershipcategory] = new OptionSetValue((int)Enums.MembershipCategory.Member);
                membership[Constants.crmd_iscompanymembership] = true;
                membership[Constants.adx_administrativecontactid] = new EntityReference(Constants.contact, order.CustomerId);
                membership[Constants.crmd_numberpurchasedmemberships] = Convert.ToInt32(product.Quantity);

                membership.Id = service.Create(membership);

                return ToMembershipModel(membership);
            }
            else
            {
                return null;
            }                    
        }

        public static void UpdateChildMembershipRenewal(IOrganizationService service, ITracingService tracing, Membership currentMembership)
        {
            // retrieve child membership
            tracing.Trace($"Retrieve child membership of {currentMembership.Id}.");

            EntityCollection memberships = service.RetrieveMultiple(new QueryExpression
            {
                EntityName = "crmd_membership",
                ColumnSet = new ColumnSet("crmd_name"),
                Criteria =
                {
                    Conditions =
                    {
                        new ConditionExpression("crmd_parentcompanymembershipid", ConditionOperator.Equal, currentMembership.Id),
                        new ConditionExpression("crmd_membershipstatus", ConditionOperator.In, new Int32[] { 790410000, 790410003 }),
                        new ConditionExpression("crmd_membershiptype", ConditionOperator.Equal, 790410000),
                    }
                }
            });

            tracing.Trace($"Retrieved {memberships.Entities.Count} child membership(s).");

            // update child membership renewal date
            tracing.Trace($"Update child membership renewal date to {currentMembership.RenewalDate}.");
            foreach (Entity membership in memberships.Entities)
            {
                membership[Constants.adx_renewaldate] = currentMembership.RenewalDate;
                membership[Constants.crmd_membershipaction] = new OptionSetValue(790410001); // Renewal
                service.Update(membership);
            }
        }
        #endregion

        #region "Private Methods"
        private static Membership ToMembershipModel(Entity membershipEntity)
        {
            if(membershipEntity != null)
            {
                Membership membership = new Membership()
                {
                    Id = membershipEntity.Id,
                    StartDate = membershipEntity.GetAttributeValue<DateTime>(Constants.crmd_startdate),
                    RenewalDate = membershipEntity.GetAttributeValue<DateTime>(Constants.adx_renewaldate)
                };

                if (membershipEntity.HasAttributeValue(Constants.crmd_membershipstatus))
                {
                    membership.MembershipStatus = membershipEntity.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipstatus).Value;
                }

                if (membershipEntity.HasAttributeValue(Constants.crmd_membershipcategory))
                {
                    membership.MembershipCategory = membershipEntity.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipcategory).Value;
                }

                if(membershipEntity.HasAttributeValue(Constants.crmd_membershiptype))
                {
                    membership.MembershipType = membershipEntity.GetAttributeValue<OptionSetValue>(Constants.crmd_membershiptype).Value;
                }
                
                if (membershipEntity.HasAttributeValue(Constants.crmd_autorenew))
                {
                    membership.IsAutoRenewMembership = membershipEntity.GetAttributeValue<bool>(Constants.crmd_autorenew);
                }

                if (membershipEntity.HasAttributeValue(Constants.crmd_membershipaction))
                {
                    membership.MembershipAction = membershipEntity.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipaction).Value;
                }

                if(membership.MembershipType == (int)Enums.MembershipType.OrganizationPaid)
                {
                    if (membershipEntity.HasAttributeValue(Constants.adx_accountid))
                    {
                        EntityReference accountLookup = membershipEntity.GetAttributeValue<EntityReference>(Constants.adx_accountid);
                        membership.AccountId = accountLookup.Id;

                        membership.NumberOfPurchasedMemberships = membershipEntity.GetAttributeValue<int>(Constants.crmd_numberpurchasedmemberships);
                        membership.NumberOfActiveMembership = membershipEntity.GetAttributeValue<int>(Constants.crmd_numactivememberships);
                    }
                }
                else
                {
                    if (membershipEntity.HasAttributeValue(Constants.crmd_contactid))
                    {
                        EntityReference contactLookup = membershipEntity.GetAttributeValue<EntityReference>(Constants.crmd_contactid);
                        membership.ContactId = contactLookup.Id;
                    }
                }
               
                EntityReference productLookup = membershipEntity.GetAttributeValue<EntityReference>(Constants.crmd_productlkup);
                if (productLookup != null)
                {
                    membership.MembershipProductLookupId = productLookup.Id;
                }

                return membership;
            }

            return null;           
        }
        #endregion
    }
}
