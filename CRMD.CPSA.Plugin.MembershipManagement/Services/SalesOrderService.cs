﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using CRMD.CPSA.Plugin.MembershipManagement.Models;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;

namespace CRMD.CPSA.Plugin.MembershipManagement.Services
{
    public static class SalesOrderService
    {
        public static SalesOrder Retrieve(IOrganizationService service, Guid salesOrderId)
        {
            if(salesOrderId != Guid.Empty)
            {
                Entity orderEntity = service.Retrieve(Constants.Entity.SalesOrder, salesOrderId, new ColumnSet(Constants.name, Constants.customerid, Constants.salesorder_statuscode, Constants.totalamount,
                                                                                                        Constants.adxcm_autorenewalopted, Constants.crmd_orderpaymenttype, Constants.crmd_checknumber, 
                                                                                                        Constants.crmd_approvalcode, Constants.crmd_approvaldateandtime, Constants.crmd_membershiplkup,
                                                                                                        Constants.crmd_discount));
                if(orderEntity != null)
                {                    
                    SalesOrder order = new SalesOrder()
                    {
                        Id = orderEntity.Id,
                        Name = orderEntity.GetAttributeValue<string>(Constants.name),
                        IsComplete = (orderEntity.FormattedValues[Constants.salesorder_statuscode].ToString() == "Complete"),
                        IsAutoRenewMembership = orderEntity.GetAttributeValue<bool>(Constants.adxcm_autorenewalopted),
                        Products = GetLineItems(service, orderEntity.Id),
                        TotalAmount = orderEntity.GetAttributeValue<Money>(Constants.totalamount).Value,
                        ApprovalCode = orderEntity.GetAttributeValue<string>(Constants.crmd_approvalcode),
                        ApprovalDateAndTime = orderEntity.GetAttributeValue<DateTime>(Constants.crmd_approvaldateandtime),
                    };

                    EntityReference customerLookup = orderEntity.GetAttributeValue<EntityReference>(Constants.customerid);
                    if (customerLookup != null)
                    {
                        order.CustomerId = customerLookup.Id;

                        Guid contactParentCustomerId = ContactService.GetContactParentCustomerId(service, order.CustomerId);
                        if (contactParentCustomerId != Guid.Empty)
                        {
                            order.CorporateAccount = AccountService.GetAccount(service, contactParentCustomerId);
                        };
                    }

                    if (orderEntity.HasAttributeValue(Constants.crmd_orderpaymenttype))
                    {
                        order.PaymentType = (Enums.OrderPaymentType)orderEntity.GetAttributeValue<OptionSetValue>(Constants.crmd_orderpaymenttype).Value;
                        if (order.PaymentType == Enums.OrderPaymentType.Check)
                        {
                            order.CheckNumber = orderEntity.GetAttributeValue<string>(Constants.crmd_checknumber);
                        }
                    }

                    EntityReference orderDiscount = orderEntity.GetAttributeValue<EntityReference>(Constants.crmd_discount);
                    if(orderDiscount != null)
                    {
                        Entity discount = service.Retrieve(Constants.crmd_discount, orderDiscount.Id, new ColumnSet(Constants.crmd_membershiptype, Constants.crmd_membershipcategory));
                        if(discount != null)
                        {
                            if (discount.HasAttributeValue(Constants.crmd_membershiptype))
                            {
                                order.OrderDiscountMembershipType = discount.GetAttributeValue<OptionSetValue>(Constants.crmd_membershiptype).Value;
                            }

                            if (discount.HasAttributeValue(Constants.crmd_membershipcategory)){
                                order.OrderDiscountMembershipCategory = discount.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipcategory).Value;
                            }
                        }
                    }

                    EntityReference membershipLookup = orderEntity.GetAttributeValue<EntityReference>(Constants.crmd_membershiplkup);
                    if(membershipLookup != null)
                    {
                        order.MembershipLookupId = membershipLookup.Id;
                    }
                    
                    return order;
                }
            }
            
            return null;
        }

        public static void UpdateOrder(IOrganizationService service, ITracingService tracing, Guid orderId, Membership membership, SalesOrderDetail membershipProductLineItem, bool isNewMembership)
        {
            //Current order status is Fulfilled which makes the fields readonly. Activate the order first, update the order with membership lookup, then set it back to fulfilled
            ActivateOrder(service, orderId);
            tracing.Trace("Order status changed from Fulfilled to Active");

            Entity orderEntity = new Entity(Constants.Entity.SalesOrder, orderId);
            orderEntity[Constants.crmd_membershiplkup] = new EntityReference(Constants.crmd_membership, membership.Id);

            // update the Event Registered field to prevent Event registration plugin to run again. 
            orderEntity["crmd_eventregistered"] = true;

            service.Update(orderEntity);
            tracing.Trace("Updated order with membership lookup");

            UpdateMembershipLineItemStartEndDate(service, tracing, membership, membershipProductLineItem, isNewMembership);

            FulfillOrder(service, orderId);
            tracing.Trace("Order status changed from Active to Fulfilled");
        }

        public static void UpdateMembershipLineItemStartEndDate(IOrganizationService service, ITracingService tracing, Membership membership, SalesOrderDetail membershipProductLineItem, bool isNewMembership)
        {
            Entity membershipLineItem = new Entity(Constants.Entity.SalesOrderDetail, membershipProductLineItem.Id);
            DateTime startDate = isNewMembership ? ((membershipProductLineItem.ProRateStartDate != null && membershipProductLineItem.ProRateStartDate > DateTime.MinValue) ? membershipProductLineItem.ProRateStartDate : membership.StartDate) : 
                                                   membership.RenewalDate.AddMonths(membershipProductLineItem.ProductMembershipTerm * -1).AddDays(1);

            if(membershipProductLineItem.ProRateStartDate != null && membershipProductLineItem.ProRateStartDate > DateTime.MinValue)
            {
                startDate = membershipProductLineItem.ProRateStartDate;
            }

            membershipLineItem[Constants.crmd_startdate] = startDate;
            membershipLineItem[Constants.crmd_enddate] = membership.RenewalDate;
            service.Update(membershipLineItem);
            tracing.Trace("Updated order line item start date and end date");
        }

        public static List<SalesOrderDetail> RetrieveProductLineItems(IOrganizationService service, Guid salesOrderId)
        {
            return GetLineItems(service, salesOrderId);
        }

        public static void ActivateOrder(IOrganizationService service, Guid salesOrderId)
        {
            CrmHelper.SetState(service, Constants.Entity.SalesOrder, salesOrderId, (int)Enums.OrderState.Active, (int)Enums.ActiveOrderStatusReason.New);
        }

        public static void FulfillOrder(IOrganizationService service, Guid salesOrderId)
        {
            Entity orderClose = new Entity(Constants.orderclose);
            orderClose[Constants.salesorderid] = new EntityReference(Constants.Entity.SalesOrder, salesOrderId);

            var request = new FulfillSalesOrderRequest
            {
                OrderClose = orderClose,
                Status = new OptionSetValue((int)Enums.FulfilledOrderStatusReason.Complete)
            };

            service.Execute(request);
        }

        #region "Private Methods"
        private static List<SalesOrderDetail> GetLineItems(IOrganizationService service, Guid salesOrderId)
        {
            List<SalesOrderDetail> lstLineItems = new List<SalesOrderDetail>();

            string query = @"<fetch mapping='logical' version='1.0' >
                                <entity name='salesorderdetail' >
                                    <attribute name='salesorderdetailid' />
                                    <attribute name='productid' />
                                    <attribute name='crmd_discount' />
                                    <attribute name='quantity' />
                                    <attribute name='priceperunit' />
                                    <attribute name='crmd_proratestartdate' />
                                    <order attribute='createdon' descending='false'/>
                                    <filter type='and' >
                                        <condition attribute='salesorderid' operator='eq' value='" + salesOrderId + @"' />
                                    </filter>
                                    <link-entity name='crmd_discount' from='crmd_discountid' to='crmd_discount' alias='discount' link-type='outer'>
                                        <attribute name='crmd_membershiptype' />
                                        <attribute name='crmd_membershipcategory' />
                                    </link-entity>
                                    <link-entity name='product' from='productid' to='productid' alias='prod'>
                                        <attribute name='name' />
                                        <attribute name='productid' />
                                        <attribute name='productnumber' />
                                        <attribute name='producttypecode' />
                                        <attribute name='crmd_membershiptype' />
                                        <attribute name='crmd_membershipcategory' />
                                        <attribute name='crmd_renewalproduct' />
                                        <link-entity name='uomschedule' from='uomscheduleid' to='defaultuomscheduleid' alias='uomsched'>
                                            <attribute name='name' />                                        
                                        </link-entity>
                                        <link-entity name='uom' from='uomid' to='defaultuomid' alias='uom'>
                                            <attribute name='quantity' />                                        
                                        </link-entity>
                                    </link-entity>
                                </entity>
                            </fetch>";

            var result = service.RetrieveMultiple(new FetchExpression(query));
            if(result != null && result.Entities != null)
            {
                foreach(var orderProduct in result.Entities)
                {
                    SalesOrderDetail lineItem = new SalesOrderDetail()
                    {
                        Id = orderProduct.Id,
                        Quantity = orderProduct.GetAttributeValue<decimal>(Constants.quantity),
                        PricePerUnit = orderProduct.GetAttributeValue<Money>(Constants.priceperunit).Value,
                        ProRateStartDate = orderProduct.GetAttributeValue<DateTime>(Constants.crmd_proratestartdate)
                    };

                    if (orderProduct.HasAttributeValue(Constants.productid))
                    {
                        EntityReference productLookup = orderProduct.GetAttributeValue<EntityReference>(Constants.productid);
                        lineItem.ProductId = productLookup.Id;

                        if (orderProduct.HasAttributeValue("prod." + Constants.name))
                        {
                            lineItem.ProductName = (string)orderProduct.GetAttributeValue<AliasedValue>("prod." + Constants.name).Value;
                        }

                        if (orderProduct.HasAttributeValue("prod." + Constants.crmd_membershipcategory))
                        {
                            lineItem.ProductMembershipCategory = ((OptionSetValue)orderProduct.GetAttributeValue<AliasedValue>("prod." + Constants.crmd_membershipcategory).Value).Value;
                        }

                        if(orderProduct.HasAttributeValue("prod." + Constants.crmd_membershiptype))
                        {
                            lineItem.ProductMembershipType = ((OptionSetValue)orderProduct.GetAttributeValue<AliasedValue>("prod." + Constants.crmd_membershiptype).Value).Value;
                        }

                        if (orderProduct.HasAttributeValue("prod." + Constants.productnumber))
                        {
                            lineItem.ProductNumber = (string)orderProduct.GetAttributeValue<AliasedValue>("prod." + Constants.productnumber).Value;
                        }

                        if (orderProduct.HasAttributeValue("prod." + Constants.producttypecode))
                        {
                            lineItem.ProductTypeCode = ((OptionSetValue)orderProduct.GetAttributeValue<AliasedValue>("prod." + Constants.producttypecode).Value).Value;
                        }

                        if (orderProduct.HasAttributeValue("uomsched." + Constants.name))
                        {
                            lineItem.ProductUnitGroupName = orderProduct.GetAttributeValue<AliasedValue>("uomsched." + Constants.name).Value.ToString();
                        }

                        if (orderProduct.HasAttributeValue("uom." + Constants.quantity))
                        {
                            lineItem.ProductDefaultUnit = (decimal)orderProduct.GetAttributeValue<AliasedValue>("uom." + Constants.quantity).Value;
                        }

                        if (orderProduct.HasAttributeValue("prod." + Constants.renewalproduct))
                        {
                            lineItem.RenewalProductId = ((EntityReference)orderProduct.GetAttributeValue<AliasedValue>("prod." + Constants.renewalproduct).Value).Id;
                        }

                        if (orderProduct.HasAttributeValue("prod." + Constants.productid))
                        {
                            lineItem.ProductNameId = (Guid)orderProduct.GetAttributeValue<AliasedValue>("prod." + Constants.productid).Value;
                        }
                    }

                    if (orderProduct.HasAttributeValue("discount." + Constants.crmd_membershiptype))
                    {
                        lineItem.DiscountMembershipType = ((OptionSetValue)orderProduct.GetAttributeValue<AliasedValue>("discount." + Constants.crmd_membershiptype).Value).Value;
                    }

                    if (orderProduct.HasAttributeValue("discount." + Constants.crmd_membershipcategory))
                    {
                        lineItem.DiscountMembershipCategory = ((OptionSetValue)orderProduct.GetAttributeValue<AliasedValue>("discount." + Constants.crmd_membershipcategory).Value).Value;
                    }

                    lstLineItems.Add(lineItem);
                }
            }

            return lstLineItems;
        }
        #endregion
    }
}
