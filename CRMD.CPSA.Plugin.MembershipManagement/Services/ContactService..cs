﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using CRMD.CPSA.Plugin.MembershipManagement.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.MembershipManagement.Services
{
    public static class ContactService
    {
        public static void UpdateContactWithCurrentMembership(IOrganizationService service, Guid contactId, Membership currentMembership)
        {
            if (contactId != Guid.Empty && currentMembership != null)
            {
                Entity currentContact = service.Retrieve(Constants.contact, contactId, new ColumnSet(Constants.crmd_membershiptype, Constants.crmd_membershipcat, Constants.crmd_membershipstatus,
                                                                                                     Constants.crmd_membershipdatefirstjoined, Constants.crmd_nextrenewaldate, Constants.crmd_autorenewmembership,
                                                                                                     Constants.crmd_intrialperiod, Constants.crmd_trialmembershipstatus, Constants.crmd_trialstartdate, Constants.crmd_trialenddate,
                                                                                                     Constants.crmd_membershipcancelledondate, Constants.crmd_trialcancelreason, Constants.crmd_cancellationnotes, Constants.crmd_trialperiodlength,
                                                                                                     Constants.crmd_implied, Constants.crmd_impliedexpirydate));



                if (currentContact != null && 
                    (!currentContact.HasAttributeValue(Constants.crmd_membershiptype) || (currentContact.HasAttributeValue(Constants.crmd_membershiptype) && currentContact.HasAttributeValue(Constants.crmd_membershipstatus) &&
                                                                                            !(currentContact.GetAttributeValue<OptionSetValue>(Constants.crmd_membershiptype).Value == (int)Enums.MembershipType.OrganizationPaid &&
                                                                                                (currentContact.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipstatus).Value == (int)Enums.MembershipStatus.Current ||
                                                                                                currentContact.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipstatus).Value == (int)Enums.MembershipStatus.GracePeriod)))))

                {
                    Entity contactToUpdate = new Entity(Constants.contact, contactId);

                    SetAttributesForMembshipInformationUpdate(currentContact, contactToUpdate, currentMembership);
                    ClearTrialMembershipInformation(currentContact, contactToUpdate);

                    if (contactToUpdate.Attributes.Count > 0)
                    {
                        service.Update(contactToUpdate);
                    }
                }
            }
        }

        public static Guid GetContactParentCustomerId(IOrganizationService service, Guid contactId)
        {
            Entity contact = service.Retrieve(Constants.contact, contactId, new ColumnSet(Constants.parentcustomerid));
            if (contact != null)
            {
                if (contact.HasAttributeValue(Constants.parentcustomerid))
                {
                    return contact.GetAttributeValue<EntityReference>(Constants.parentcustomerid).Id;
                }
            }

            return Guid.Empty;
        }

        #region "Private Methods"
        private static int GetAutoRenewMembershipOptionSetFromBool(bool isAutoRenewMembership)
        {
            return isAutoRenewMembership ? (int)Enums.AutoRenewMembership.Yes : (int)Enums.AutoRenewMembership.No;
        }

        private static void SetAttributesForMembshipInformationUpdate(Entity currentContact, Entity contactToUpdate, Membership currentMembership)
        {
            if (!currentContact.HasAttributeValue(Constants.crmd_membershiptype) ||
                    (currentContact.HasAttributeValue(Constants.crmd_membershiptype) && currentContact.GetAttributeValue<OptionSetValue>(Constants.crmd_membershiptype).Value != currentMembership.MembershipType))
            {
                contactToUpdate[Constants.crmd_membershiptype] = new OptionSetValue(currentMembership.MembershipType);
            }

            if (!currentContact.HasAttributeValue(Constants.crmd_membershipcat) ||
                (currentContact.HasAttributeValue(Constants.crmd_membershipcat) && currentContact.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipcat).Value != currentMembership.MembershipCategory))
            {
                contactToUpdate[Constants.crmd_membershipcat] = new OptionSetValue(currentMembership.MembershipCategory);
            }

            if (!currentContact.HasAttributeValue(Constants.crmd_membershipstatus) ||
                (currentContact.HasAttributeValue(Constants.crmd_membershipstatus) && currentContact.GetAttributeValue<OptionSetValue>(Constants.crmd_membershipstatus).Value != currentMembership.MembershipStatus))
            {
                contactToUpdate[Constants.crmd_membershipstatus] = new OptionSetValue(currentMembership.MembershipStatus);
            }

            if (!currentContact.HasAttributeValue(Constants.crmd_membershipdatefirstjoined) ||
                (currentContact.HasAttributeValue(Constants.crmd_membershipdatefirstjoined) && currentContact.GetAttributeValue<DateTime>(Constants.crmd_membershipdatefirstjoined) != currentMembership.StartDate))
            {
                contactToUpdate[Constants.crmd_membershipdatefirstjoined] = currentMembership.StartDate;
            }

            if (!currentContact.HasAttributeValue(Constants.crmd_nextrenewaldate) ||
                (currentContact.HasAttributeValue(Constants.crmd_nextrenewaldate) && currentContact.GetAttributeValue<DateTime>(Constants.crmd_nextrenewaldate) != currentMembership.RenewalDate))
            {
                contactToUpdate[Constants.crmd_nextrenewaldate] = currentMembership.RenewalDate;
            }
                        
            if (!currentContact.HasAttributeValue(Constants.crmd_impliedexpirydate) ||
                (currentContact.HasAttributeValue(Constants.crmd_impliedexpirydate) && DateTime.Now.AddYears(2) > currentContact.GetAttributeValue<DateTime>(Constants.crmd_impliedexpirydate)))
            {
                contactToUpdate[Constants.crmd_impliedexpirydate] = DateTime.Now.AddYears(2);
                contactToUpdate[Constants.crmd_implied] = true;
            }
            else if (currentContact.HasAttributeValue(Constants.crmd_implied))
            {
                contactToUpdate[Constants.crmd_implied] = true;
            }
            
        }

        private static void ClearTrialMembershipInformation(Entity currentContact, Entity contactToUpdate)
        {
            if (currentContact.HasAttributeValue(Constants.crmd_intrialperiod) && currentContact.GetAttributeValue<bool>(Constants.crmd_intrialperiod))
            {
                contactToUpdate[Constants.crmd_intrialperiod] = false;
            }

            if (currentContact.HasAttributeValue(Constants.crmd_trialmembershipstatus))
            {
                contactToUpdate[Constants.crmd_trialmembershipstatus] = null;
            }

            if (currentContact.HasAttributeValue(Constants.crmd_trialstartdate))
            {
                contactToUpdate[Constants.crmd_trialstartdate] = null;
            }

            if (currentContact.HasAttributeValue(Constants.crmd_trialenddate))
            {
                contactToUpdate[Constants.crmd_trialenddate] = null;
            }

            if (currentContact.HasAttributeValue(Constants.crmd_membershipcancelledondate))
            {
                contactToUpdate[Constants.crmd_membershipcancelledondate] = null;
            }

            if (currentContact.HasAttributeValue(Constants.crmd_trialcancelreason))
            {
                contactToUpdate[Constants.crmd_trialcancelreason] = null;
            }

            if (currentContact.HasAttributeValue(Constants.crmd_cancellationnotes))
            {
                contactToUpdate[Constants.crmd_trialperiodlength] = null;
            }
        }
        #endregion
    }
}
