﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using CRMD.CPSA.Plugin.MembershipManagement.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.MembershipManagement.Services
{
    public static class AccountService
    {
        public static Guid GetDesignatedAdminLookup(IOrganizationService service, Guid accountId)
        {
            Entity account = service.Retrieve(Constants.account, accountId, new ColumnSet(Constants.primarycontactid));
            if(account != null)
            {
                if (account.HasAttributeValue(Constants.primarycontactid))
                {
                    return account.GetAttributeValue<EntityReference>(Constants.primarycontactid).Id;
                }
            }

            return Guid.Empty;
        }

        public static void UpdateCurrentMembership(IOrganizationService service, Guid accountId, Guid membershipId)
        {
            if(accountId != Guid.Empty && membershipId != Guid.Empty)
            {
                Entity account = new Entity(Constants.account, accountId);
                account[Constants.crmd_currentmembershiplkup] = new EntityReference(Constants.crmd_membership, membershipId);

                service.Update(account);
            }
        }

        public static Account GetAccount(IOrganizationService service, Guid accountId)
        {
            Entity accountEntity = service.Retrieve(Constants.account, accountId, new ColumnSet(Constants.accountid, Constants.primarycontactid, Constants.crmd_companydiscount));
            if(accountEntity != null)
            {
                Account account = new Account()
                {
                    Id = accountEntity.Id,
                    DesignatedAdminId = accountEntity.HasAttributeValue(Constants.primarycontactid) ? accountEntity.GetAttributeValue<EntityReference>(Constants.primarycontactid).Id : Guid.Empty,
                    CompanyDiscount = accountEntity.HasAttributeValue(Constants.crmd_companydiscount) ? accountEntity.GetAttributeValue<decimal>(Constants.crmd_companydiscount) : 0m
                };

                var couponCodes = GetCouponCodes(service, accountId);
                account.HasCouponCodes = (couponCodes != null && couponCodes.Count > 0);

                return account;
            }

            return null;
        }

        private static List<Entity> GetCouponCodes(IOrganizationService service, Guid accountId)
        {
            var fetchXML = @"<fetch mapping='logical' version='1.0' >
                                <entity name='crmd_account_crmd_discount'>
                                    <filter type='and'>
                                        <condition attribute='accountid' operator='eq' value='" + accountId.ToString() + @"' />
                                    </filter>
                                </entity>
                            </fetch>";

            var result = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if(result != null && result.Entities != null)
            {
                return result.Entities.ToList();
            }

            return null;
        }
    }
}
