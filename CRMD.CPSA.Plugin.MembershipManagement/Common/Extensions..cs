﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.MembershipManagement.Common
{
    public static class Extensions
    {
        public static bool HasAttributeValue(this Entity entity, string attributeName)
        {
            return (entity != null && entity.Attributes.Contains(attributeName) && entity.Attributes[attributeName] != null);
        }
    }
}
