﻿namespace CRMD.CPSA.Plugin.MembershipManagement.Common
{
    public class Constants
    {
        public class Entity
        {
            public const string SalesOrder = "salesorder";
            public const string SalesOrderDetail = "salesorderdetail";
        }

        //Contact
        public const string contact = "contact";
        public const string crmd_membershipcat = "crmd_membershipcat";
        public const string crmd_nextrenewaldate = "crmd_nextrenewaldate";
        public const string crmd_membershipdatefirstjoined = "crmd_membershipdatefirstjoined";
        public const string crmd_autorenewmembership = "crmd_autorenewmembership";
        public const string parentcustomerid = "parentcustomerid";
        public const string crmd_impliedexpirydate = "crmd_impliedexpirydate";
        public const string crmd_implied = "crmd_implied";

        //SalesOrder 
        public const string customerid = "customerid";
        public const string salesorder_statuscode = "statuscode";
        public const string crmd_membershiplkup = "crmd_membershiplkup";
        public const string crmd_approvalcode = "crmd_approvalcode";
        public const string crmd_approvaldateandtime = "crmd_approvaldateandtime";
        public const string totalamount = "totalamount";
        public const string adxcm_autorenewalopted = "adxcm_autorenewalopted";
        public const string crmd_orderpaymenttype = "crmd_orderpaymenttype";
        public const string crmd_checknumber = "crmd_checknumber";
        public const string datefulfilled = "datefulfilled";
        
        //Membership
        public const string crmd_membership = "crmd_membership";
        public const string crmd_membershiptype = "crmd_membershiptype";
        public const string crmd_membershipstatus = "crmd_membershipstatus";
        public const string crmd_contactid = "crmd_contactid";
        public const string crmd_productlkup = "crmd_productlkup";
        public const string crmd_startdate = "crmd_startdate";
        public const string crmd_membershipcategory = "crmd_membershipcategory";
        public const string adx_renewaldate = "adx_renewaldate";
        public const string crmd_autorenew = "crmd_autorenew";
        public const string crmd_membershipaction = "crmd_membershipaction";

        public const string crmd_intrialperiod = "crmd_intrialperiod"; 
        public const string crmd_trialmembershipstatus = "crmd_trialmembershipstatus";
        public const string crmd_trialstartdate = "crmd_trialstartdate";
        public const string crmd_trialenddate = "crmd_trialenddate";
        public const string crmd_membershipcancelledondate = "crmd_membershipcancelledondate";
        public const string crmd_trialcancelreason = "crmd_trialcancelreason";
        public const string crmd_cancellationnotes = "crmd_cancellationnotes";
        public const string crmd_trialperiodlength = "crmd_trialperiodlength";

        public const string adx_accountid = "adx_accountid";
        public const string crmd_iscompanymembership = "crmd_iscompanymembership";
        public const string adx_administrativecontactid = "adx_administrativecontactid";
        public const string crmd_numberpurchasedmemberships = "crmd_numberpurchasedmemberships";
        public const string crmd_numactivememberships = "crmd_numactivememberships";

        //SalesOrderDetail
        public const string crmd_discount = "crmd_discount";
        public const string priceperunit = "priceperunit";
        public const string ispriceoverridden = "ispriceoverridden";
        public const string crmd_proratestartdate = "crmd_proratestartdate";
        public const string crmd_enddate = "crmd_enddate";

        //Product
        public const string product = "product";
        public const string productid = "productid";
        public const string producttypecode = "producttypecode";
        public const string defaultuomscheduleid = "defaultuomscheduleid";
        public const string defaultuomid = "defaultuomid";
        public const string productnumber = "productnumber";
        public const string renewalproduct = "crmd_renewalproduct";

        //Unit
        public const string quantity = "quantity";
        
        //Payment
        public const string crmd_payment = "crmd_payment";
        public const string crmd_name = "crmd_name";
        public const string crmd_paymentdate = "crmd_paymentdate";
        public const string crmd_paymentamount = "crmd_paymentamount";
        public const string crmd_paymenttype = "crmd_paymenttype";
        public const string crmd_orderid = "crmd_orderid";
        public const string payment_statuscode = "statuscode";
        public const string crmd_referencenumber = "crmd_referencenumber";

        //OrderClose
        public const string orderclose = "orderclose";
        public const string salesorderid = "salesorderid";

        //Account
        public const string account = "account";
        public const string accountid = "accountid";
        public const string primarycontactid = "primarycontactid";
        public const string crmd_currentmembershiplkup = "crmd_currentmembershiplkup";
        public const string crmd_companydiscount = "crmd_companydiscount";
       
        public const string name = "name";
        public const string statecode = "statecode";
        public const int StateCode_Active = 0;

        public const string UnitGroup_Individual = "Individual";
        public const string UnitGroup_Corporate = "Corporate";
    }
}
