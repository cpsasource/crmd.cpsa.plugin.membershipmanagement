﻿using System;
using System.Linq;

namespace CRMD.CPSA.Plugin.MembershipManagement.Common
{
    public class EnumHelper
    {
        public static T GetEnum<T>(int value)
        {
            return Enum.GetValues(typeof(T))
                    .Cast<T>()
                    .Where(c => c.GetHashCode() == value)
                    .FirstOrDefault();
        }
    }
}
