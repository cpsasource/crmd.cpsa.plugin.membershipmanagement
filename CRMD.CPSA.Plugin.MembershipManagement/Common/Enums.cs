﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.MembershipManagement.Common
{
    public static class Enums
    {
        public enum MembershipType
        {
            OrganizationPaid = 790410000,
            Subscriber = 790410001,
            SubscriberPaid = 103460000
        }

        public enum MembershipStatus
        {
            Current = 790410000,
            Lapsed = 790410001,
            Canceled = 790410005,
            GracePeriod = 790410003
        }

        public enum MembershipCategory
        {
            Saver = 103460002,
            Member = 100000000,
            Life = 790410004,
            Student = 103460001,
            Trial = 103460000
        }

        public enum PaymentType
        {
            CreditCard = 790410000,
            Cheque = 790410001,
            Cash = 790410002,
            OnAccount = 103460000
        }

        public enum PaymentStatus
        {
            Pending = 1,
            Paid = 103460000
        }

        public enum AutoRenewMembership
        {
            Yes = 790410000,
            No = 790410001
        }

        public enum OrderState
        {
            Active = 0,
            Submitted = 1,
            Canceled = 2,
            Fulfilled = 3,
            Invoiced = 4
        }

        public enum ActiveOrderStatusReason
        {
            New = 1,
            Pending = 2
        }

        public enum FulfilledOrderStatusReason
        {
            Complete = 100001,
            Partial = 100002
        }

        public enum OrderPaymentType
        {
            Online = 103460000,
            Check = 103460001,
            Cash = 100000000,
            OnAccount = 103460002
        }

        public enum MembershipAction
        {
            New = 790410000,
            Renewal = 790410001
        }
    }
}
