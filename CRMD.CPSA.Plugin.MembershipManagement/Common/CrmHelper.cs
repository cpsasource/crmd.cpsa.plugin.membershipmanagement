﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.MembershipManagement.Common
{
    public static class CrmHelper
    {
        public static void SetState(IOrganizationService service, string entityName, Guid entityId, int state, int status)
        {
            SetStateRequest setState = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(entityName, entityId),
                State = new OptionSetValue((int)state),
                Status = new OptionSetValue((int)status)
            };

            service.Execute(setState);
        }
    }
}
