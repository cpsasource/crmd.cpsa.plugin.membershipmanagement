﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using CRMD.CPSA.Plugin.MembershipManagement.Models;
using CRMD.CPSA.Plugin.MembershipManagement.Services;
using Microsoft.Xrm.Sdk;
using System;

namespace CRMD.CPSA.Plugin.MembershipManagement
{
    public class ManageIndividualMembership : PluginBase, IPlugin
    {
        //Update of SalesOrder, Post-Op
        public void Execute(IServiceProvider serviceProvider)
        {
            base.Initialize(serviceProvider);

            if (this.TargetEntity == null)
                return;

            if (this.Context.Depth > 2)
                return;

            this.Tracing.Trace("*** Begin : " + this.TargetEntity.Id + "***");

            SalesOrder order = SalesOrderService.Retrieve(this.Service, this.TargetEntity.Id);
            if (order != null && order.WillProcessIndividualMembership)
            {
                this.Tracing.Trace("Plugin will process Individual Membership");

                Membership latestMembership = null;
                bool isNewMembership = false;

                //Orders created from Order Form, not Renew Membership workflow
                if (order.MembershipLookupId == Guid.Empty)
                {
                    Membership currentMembership = MembershipService.GetLatestIndividualMembership(this.Service, order.CustomerId);

                    if (currentMembership != null && currentMembership.IsCurrentOrGracePeriod)
                    {
                        latestMembership = MembershipService.UpdateIndividualMembershipAsLatestCurrent(this.Service, currentMembership, order);
                        this.Tracing.Trace("Updating customer's latest current membership : " + latestMembership.Id);
                    }
                    else
                    {
                        isNewMembership = true;
                        Membership newMembership = MembershipService.CreateNewIndividualMembership(this.Service, order);
                        if (newMembership != null && newMembership.Id != Guid.Empty)
                        {
                            this.Tracing.Trace("Created a new membership for this customer : " + newMembership.Id);
                            latestMembership = newMembership;
                        }
                    }

                    if (latestMembership != null)
                    {
                        ContactService.UpdateContactWithCurrentMembership(this.Service, order.CustomerId, latestMembership);
                        this.Tracing.Trace("Contact updated with current membership details : " + order.CustomerId);

                        SalesOrderService.UpdateOrder(this.Service, this.Tracing, order.Id, latestMembership, order.IndividualMembershipLineItem, isNewMembership);
                        this.Tracing.Trace("Order updated with latest membership : " + latestMembership.Id);
                    }
                }
                else // Orders created from Renew Membership workflow
                {
                    this.Tracing.Trace("Processing contact update for membership renewal...");

                    latestMembership = MembershipService.GetMembershipById(this.Service, order.MembershipLookupId);
                    if(latestMembership != null)
                    {
                        ContactService.UpdateContactWithCurrentMembership(this.Service, order.CustomerId, latestMembership);
                        this.Tracing.Trace("Contact updated with current membership details : " + order.CustomerId);
                    }
                }
            }
            else
            {
                this.Tracing.Trace(String.Format("Criteria not met for processing individual membership. Completed : {0}, Has Individual Membership Line Item: {1}", 
                                                 order.IsComplete ? "Y" : "N",
                                                 order.IndividualMembershipLineItem != null ? "Y" : "N"));
            }

            this.Tracing.Trace("*** End of plugin execution ***");
        }
    }
}
