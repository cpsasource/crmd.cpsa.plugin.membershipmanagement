﻿using CRMD.CPSA.Plugin.MembershipManagement.Common;
using CRMD.CPSA.Plugin.MembershipManagement.Models;
using CRMD.CPSA.Plugin.MembershipManagement.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace CRMD.CPSA.Plugin.MembershipManagement
{
    public class CalculateCorporateProratedMembership : PluginBase, IPlugin
    {
        // update of SalesOrderDetail, Post-Op
        public void Execute(IServiceProvider serviceProvider)
        {
            base.Initialize(serviceProvider);

            if (this.TargetEntity == null || this.Context.Depth > 2)
                return;

            if (this.Context.MessageName.ToLower() != "create")
                return;

            int newMembershipProductType = 100000004;
            this.Tracing.Trace("*** Begin : " + this.TargetEntity.Id + "***");

            this.Tracing.Trace("Retrieve sales order detail.");
            Entity salesOrderDetail = new Entity(Constants.Entity.SalesOrderDetail);
            try
            {
                salesOrderDetail = this.Service.Retrieve(Constants.Entity.SalesOrderDetail, this.TargetEntity.Id, new ColumnSet(Constants.salesorderid));
            }
            catch (Exception e)
            {
                //If order product is not unique, WorkflowAssembly deletes the created OrderProduct in real time and updates existing unique OrderProduct quantity.
            }

            if (salesOrderDetail != null && salesOrderDetail.Id != Guid.Empty)
            {
                Guid salesOrderId = salesOrderDetail.GetAttributeValue<EntityReference>(Constants.salesorderid).Id;
                this.Tracing.Trace($"Retrieve sales order by Id: {salesOrderId}");
                SalesOrder order = SalesOrderService.Retrieve(this.Service, salesOrderId);

                if (order != null && order.CorporateAccount != null
                    && order.CorporateMembershipLineItem != null && order.CorporateMembershipLineItem.ProductTypeCode == newMembershipProductType)
                {
                    // get latest corporate membership
                    this.Tracing.Trace("Retrieve account's current corporate membership");
                    Membership currentMembership = MembershipService.GetLatestCorporateMembership(this.Service, order.CorporateAccount.Id);

                    if (currentMembership != null)
                    {
                        Enums.MembershipStatus membershipStatus = EnumHelper.GetEnum<Enums.MembershipStatus>(currentMembership.MembershipStatus);
                        this.Tracing.Trace($"Account's corporate membership Id: {currentMembership.Id}, Status: {membershipStatus}");

                        // calculate pro-rated for additional seats
                        if (currentMembership.MembershipStatus == (int)Enums.MembershipStatus.Current)
                        {
                            this.CalculateProratedCostForAdditionalSeats(order.CorporateMembershipLineItem, currentMembership.RenewalDate);
                        }
                    }
                }
            }
        }

        #region Helper methods

        private void CalculateProratedCostForAdditionalSeats(SalesOrderDetail lineItem, DateTime renewalDate)
        {
            this.Tracing.Trace("Calculate prorated cost for line item.");

            // calculate month difference
            DateTime today = DateTime.Now;
            double diff = renewalDate.Subtract(today).Days / (365.25 / 12);
            int monthDiff = Convert.ToInt32(diff);
            if (diff > monthDiff)
                monthDiff++;

            // calculate prorated amount
            decimal proratedAmount = lineItem.PricePerUnit * monthDiff / 12;

            this.Tracing.Trace($"Update line item Unit Price. Original: {lineItem.PricePerUnit}, Prorated: {proratedAmount:0.##}, Renewal Date: {renewalDate:MMM dd, yyyy}, Month: {monthDiff}");
            Entity lineItemToUpdate = new Entity(Constants.Entity.SalesOrderDetail, lineItem.Id);
            lineItemToUpdate[Constants.ispriceoverridden] = true; // Override Price
            lineItemToUpdate[Constants.priceperunit] = new Money(proratedAmount);

            lineItemToUpdate[Constants.crmd_proratestartdate] = DateTime.Now;
            lineItemToUpdate[Constants.crmd_enddate] = renewalDate;
            this.Service.Update(lineItemToUpdate);
        }

        #endregion
    }
}
